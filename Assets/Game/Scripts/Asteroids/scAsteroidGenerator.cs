﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class scAsteroidGenerator : MonoBehaviour {

    //size of the asteroid
    private float radius = 8;

    //size asteroid generation is balanced against
    private float scaleBase = 8;

    //Useable mutable vector
    private Vector2 workVector = new Vector2();

    //All verticies across mesh layers
    private List<GameObject> points = new List<GameObject>();

	// Use this for initialization
	void Start () {

        radius = Random.Range(5, 10);

        List<Vector3> vertexList;
        List<int> triangleList;
        List<Vector2> uvList;
        List<Color> colorList;

        generateAsteroidMesh(radius, 40,out vertexList,out triangleList,out uvList, out colorList);

        this.GetComponent<scProceduralMesh>().createMesh(vertexList, triangleList, uvList, colorList);
        scGenerateAsteroidTexture textureGenerator = new scGenerateAsteroidTexture();

        Texture2D tex = textureGenerator.generateTexture();

        this.renderer.material.SetTexture("_Texture1", tex);
        this.renderer.material.mainTexture = tex;

	}

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0)){
            Application.LoadLevel(Application.loadedLevel);
        }

        transform.Rotate(Vector3.forward, 10 * Time.deltaTime);
	}

    private void generateAsteroidMesh(float _radius, int _numOfPoints,
        out List<Vector3> _vertexList, 
        out List<int> _triangleList, 
        out List<Vector2> _uvList,
        out List<Color> _colorList){

            float scaleFactor = calculateScaleFactor(_radius);
            _numOfPoints = calculateNumOfPoints(_numOfPoints, scaleFactor);

            List<Vector3> vertexList = new List<Vector3>();
            List<Vector2> uvList = new List<Vector2>();
            List<Color> colorList = new List<Color>();

            GameObject middleAsteroidPoint = (GameObject)GameObject.Instantiate(Resources.Load("Prefabs/VertexPoint"));
            middleAsteroidPoint.transform.position = transform.position;
            vertexList.Add(middleAsteroidPoint.transform.position - transform.position);
            uvList.Add(calculateUV(middleAsteroidPoint, transform.position, _radius));

            int layers = 5;
            float scalePerc = (_radius / layers);
            float size = _radius / scalePerc;

            for (int i = 0; i < layers; i++){
                size = (i + 1) * (_radius / layers);
                createShape(size, _numOfPoints, calculateScaleFactor(size),vertexList, uvList, out vertexList,out uvList);
            }

            List<int> triangleList = generateAsteroidMeshTriangles(vertexList, layers);

            uvList.RemoveRange(1, uvList.Count-1); //remove all but the special middle vertex
            colorList.Add(new Color(0f, 0f, 0f, 0f)); //set the color for the special middle vertex

            Color workColor = new Color(0, 0, 0, 0);
            for (int j = 0; j < points.Count; j++){
                uvList.Add(calculateUV(points[j], transform.position, _radius));
                float dst = (Vector3.Distance(points[j].transform.position, transform.position));
                float p = dst/_radius;

                workColor.a = p;

                colorList.Add(workColor);
            }

            _vertexList = vertexList;
            _triangleList = triangleList;
            _uvList = uvList;
            _colorList = colorList;
    }

    private List<int> generateAsteroidMeshTriangles(List<Vector3> _vertexList, int layers){
        List<int> triangleList = new List<int>();

        int count = _vertexList.Count / layers; // number of verticies on each layer

        //iterate over all the layers
        for (int j = 0; j < layers; j++){

            //first ring has special logic due to the special middle vertext
            if (j == 0){
                for (int i = 1; i < count+1; i++)
                {
                    if (i == 1)
                    {
                        triangleList.Add(count);
                        triangleList.Add(i);
                        triangleList.Add(0);
                    }
                    else
                    {
                        triangleList.Add(i - 1);
                        triangleList.Add(i);
                        triangleList.Add(0);
                    }
                }
            }else{ //for all layers besides the first one
                int localCount = 0; 
                for (int i = count * j ; i < count * (j+1); i++)
                {
                    if ( i == count * j){
                        triangleList.Add(i + 1);
                        triangleList.Add(i);
                        triangleList.Add(i + count);

                        triangleList.Add(i + count);
                        triangleList.Add(i);
                        triangleList.Add(i + count -1);
                    }else{

                        triangleList.Add(i - 1);
                        triangleList.Add(i);
                        triangleList.Add(((count * j) - count) + localCount);

                        triangleList.Add(i);
                        triangleList.Add(((count * j) - count) + localCount +1);
                        triangleList.Add(((count * j) - count) + localCount);
                    }

                    localCount++;
                }
            }
       }

       return triangleList;
    }

    private void createShape(float _radius, int _numOfPoints, float _scaleFactor,List<Vector3> _inVertexList, List<Vector2> _inUvList,
        out List<Vector3> _vertexList,
        out List<Vector2> _uvList){

            List<Vector3> vertexList = _inVertexList;
            List<Vector2> uvList = _inUvList;

            Vector2 startPosition = new Vector2(transform.position.x, transform.position.y);

            //Create the edge points
            for (int i = 0; i < _numOfPoints; i++)
            {
                double current = (i * 1.0) / _numOfPoints;

                float angle = (float)(current * Mathf.PI * 2);

                workVector.x = transform.position.x + Mathf.Sin(angle) * _radius;
                workVector.y = transform.position.y + Mathf.Cos(angle) * _radius;

                GameObject asteroidPoint = (GameObject)GameObject.Instantiate(Resources.Load("Prefabs/VertexPoint"));

                Vector2 dir = workVector - startPosition;
                dir.Normalize();

                //if the number in here isnt a fraction less than zero multiply by scaleFactor instead of divide
                float length = Random.Range(-0.5f / _scaleFactor, 0.5f / _scaleFactor);

                workVector += dir * length;

                //memory allocation here because work vector is vector2, this should be changed
                asteroidPoint.transform.position = new Vector3(workVector.x, workVector.y, 0); 
                asteroidPoint.transform.parent = this.transform;

                vertexList.Add(asteroidPoint.transform.position - transform.position);

                uvList.Add(calculateUV(asteroidPoint, startPosition, _radius));

                points.Add(asteroidPoint);
            }

            _vertexList = vertexList;
            _uvList = uvList;
    }

    private Vector2 calculateUV(GameObject _asteroidPoint, Vector3 _startPosition, float _radius)
    {
        return new Vector2(0.5f + (_asteroidPoint.transform.position.x - _startPosition.x) / (2 * _radius),
                            0.5f + (_asteroidPoint.transform.position.y - _startPosition.y) / (2 * _radius));
    }

    private float calculateScaleFactor (float _radius){
        return scaleBase / _radius;
    }

    private int calculateNumOfPoints(int _numOfPoints ,float _scaleFactor){
        return  _numOfPoints / (((int)_scaleFactor == 0 ? 1 : (int)_scaleFactor));
    }

}
