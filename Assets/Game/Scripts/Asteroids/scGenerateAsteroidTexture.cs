﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class scGenerateAsteroidTexture {

    private const int DEAD = 0;
    private const int ALIVE = 1;

    public Texture2D generateTexture(){

        int texSize = 300;

        Texture2D tex = new Texture2D(texSize, texSize, TextureFormat.ARGB32, false);

        fillBaseColor(tex);

        List<Color> colors = new List<Color>();
        colors.Add(new Color(233 / 255.0f, 202 / 255.0f, 138 / 255.0f, 1));
        colors.Add(new Color(169 / 255.0f, 137 / 255.0f, 80 / 255.0f, 1));
        colors.Add(new Color(249 / 255.0f, 239 / 255.0f, 212 / 255.0f, 1));
        colors.Add(new Color(185 / 255.0f, 153 / 255.0f, 92 / 255.0f, 1));

        List<Color> temp = new List<Color>();
        int size = colors.Count;

        //randomize color order
        while (size > 0){
            int c = Random.Range(0, colors.Count);
            temp.Add(colors[c]);
            colors.RemoveAt(c);
            size = colors.Count;
        }

        //copy the randomized list into color list
        colors.AddRange(temp); 

        int[,] textureGrid0 = createCellularAutomataGrid(texSize);
        fillLayer(tex, colors[0], textureGrid0);

        int[,] textureGrid1 = createCellularAutomataGrid(texSize);
        fillLayer(tex, colors[1], textureGrid1);

        int[,] textureGrid2 = createCellularAutomataGrid(texSize);
        fillLayer(tex, colors[2], textureGrid2);

        int[,] textureGrid3 = createCellularAutomataGrid(texSize);
        fillLayer(tex, colors[3], textureGrid3);

        tex.Apply();
        return tex;
    }

    public int[,] createCellularAutomataGrid(int _texSize){
        int[,] textureGrid = new int[_texSize, _texSize];

        populateRandomGrid(textureGrid, _texSize);

        int numOfCycles = Random.Range(5,10);

        for (int cycleNum = 0; cycleNum < numOfCycles; cycleNum++){
           doCycle(textureGrid, _texSize, cycleNum);
        }

        return textureGrid;
    }

    private void populateRandomGrid(int[,] _textureGrid, int _texSize){
        for (int i = 0; i < _texSize; i++){
            for (int j = 0; j < _texSize; j++){
                int choice = Random.Range(0, 10);
                if (choice > 7){
                    _textureGrid[i, j] = ALIVE;
                }else{
                    _textureGrid[i, j] = DEAD;
                }
            }
        }
    }

    private void doCycle(int[,] _textureGrid, int _texSize, int _cycleNum){
        int cycleChange = Random.Range(1, 5);

        for (int i = 0; i < _texSize; i++){
            for (int j = 0; j < _texSize; j++){

                int aliveCount = countSurroundingAliveCells(i, j, _textureGrid, _texSize);

                if (_cycleNum < cycleChange){
                    if (_textureGrid[i,j] == ALIVE){
                         if (aliveCount >=3 ){ 
                             _textureGrid[i, j] = ALIVE;
                         }else{
                             _textureGrid[i, j] = DEAD;
                         }
                    }else{
                        if (aliveCount >=4){ 
                            _textureGrid[i, j] = ALIVE;
                        }
                        else{
                            _textureGrid[i, j] = DEAD;
                        }
                    }
                }else{
                    int randomAlive = Random.Range(2, 5);
                    int randomDead = Random.Range(1, 5);
                    if (_textureGrid[i,j] == DEAD){
                        if (aliveCount >= randomAlive){
                            _textureGrid[i, j] = ALIVE;
                        }
                    }else{
                        if (aliveCount <= randomDead){
                            _textureGrid[i, j] = DEAD;
                        }
                    }
                }
            }
        }
    }

    private int countSurroundingAliveCells(int _x, int _y , int[,] _textureGrid, int _texSize){
        int aliveCount = 0;

        if (inBounds(_x - 1, _y, _texSize)){
            if (_textureGrid[_x - 1, _y] == ALIVE) { aliveCount++; }
        }

        if (inBounds(_x + 1, _y, _texSize)){
            if (_textureGrid[_x + 1, _y] == ALIVE) { aliveCount++; }
        }

        if (inBounds(_x, _y - 1, _texSize)){
            if (_textureGrid[_x, _y - 1] == ALIVE) { aliveCount++; }
        }

        if (inBounds(_x, _y + 1, _texSize)){
            if (_textureGrid[_x, _y + 1] == ALIVE) { aliveCount++; }
        }

        if (inBounds(_x - 1, _y - 1, _texSize)){
            if (_textureGrid[_x - 1, _y - 1] == ALIVE) { aliveCount++; }
        }

        if (inBounds(_x - 1, _y + 1, _texSize)){
            if (_textureGrid[_x - 1, _y + 1] == ALIVE) { aliveCount++; }
        }

        if (inBounds(_x + 1, _y - 1, _texSize)){
            if (_textureGrid[_x + 1, _y - 1] == ALIVE) { aliveCount++; }
        }

        if (inBounds(_x + 1, _y + 1, _texSize)){
            if (_textureGrid[_x + 1, _y + 1] == ALIVE) { aliveCount++; }
        }

        return aliveCount;
    }

    private bool inBounds(int _i , int _j, int _size){
        return (_i >= 0 && _i < _size && _j >= 0 && _j < _size);
    }

    private void fillBaseColor(Texture2D _tex){
        for (int i = 0; i < _tex.width; i++){
            for (int j = 0; j < _tex.height; j++){
                _tex.SetPixel(i, j, new Color(229 / 255.0f, 194 / 255.0f, 125 / 255.0f, 1));
            }
        }
    }

    private void fillLayer(Texture2D _tex, Color _color, int[,] _layer){
        Color workColor = new Color();

        for (int i = 0; i < _tex.width; i++){
            for (int j = 0; j < _tex.height; j++){
                if (_layer[i,j] == 1){
                    Color current = _tex.GetPixel(i, j) ;

                    float r = (current.r + _color.r) / 2;
                    float g = (current.g + _color.g) / 2;
                    float b = (current.b + _color.b) / 2;

                    workColor.r = r;
                    workColor.g = g;
                    workColor.b = b;

                    _tex.SetPixel(i, j, workColor);
                }
            }
        }
    }
}
