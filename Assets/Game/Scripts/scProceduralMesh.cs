﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class scProceduralMesh : MonoBehaviour {

    private List<Vector3> vertexList;
    private List<int> triangleList;
    private List<Vector2> uvList;
    private Mesh mesh;

    public void createMesh(List<Vector3> _vertices, List<int> _triangles, List<Vector2> _uvList, List<Color> _vertexColors)
    {
        if (this.GetComponent<MeshRenderer>() == null) { this.gameObject.AddComponent<MeshRenderer>(); }
        mesh = this.gameObject.AddComponent<MeshFilter>().mesh;

        vertexList = _vertices;
        triangleList = _triangles;
        uvList = _uvList;

        mesh.Clear();
        mesh.vertices = vertexList.ToArray();
        mesh.triangles = triangleList.ToArray();
        mesh.colors = _vertexColors.ToArray();
        mesh.uv = uvList.ToArray();
        mesh.Optimize();
        mesh.RecalculateNormals();
    }

}
