﻿Shader "2-Part Vertex Blend" {
 
Properties
{
   _Texture1 ("Texture 1 (white Alpha)", 2D) = ""
}
 
SubShader
{
   BindChannels
   {
      Bind "vertex", vertex
      Bind "texcoord", texcoord
      Bind "color", color
   }
    
   Pass
   {
     Blend One One
       
     SetTexture[_Texture1] {Combine previous Lerp(primary) texture}

   }
  
}
 
}