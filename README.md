A Unity project for procedural generating 2d planets. Generates both the mesh and the texture for the mesh.

Example of generated planets:

![RandomPlanets.png](https://bitbucket.org/repo/yeMjkj/images/2882600523-RandomPlanets.png)

**Contact**

For any further questions or comments I can be contacted at nathMWilliams (at) gmail (.) com or to see other examples of my work visit www.nathanmwilliams.com